package com.example.marker.restauracje_pamo;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marker.restauracje_pamo.Restaurant;
import com.example.marker.restauracje_pamo.permission.PermissionManager;


public class RestaurantActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        Bundle extras = getIntent().getExtras();
        int id = 0;

        if(extras != null){
            id = extras.getInt("Restaurant_ID");
        }

        TextView restaurantName = (TextView) findViewById(R.id.restaurantName);
        TextView restaurantDescription = (TextView) findViewById(R.id.restaurantDescription);
        TextView restaurantLocation = (TextView) findViewById(R.id.restaurantLocation);
        ImageButton showOnMap = (ImageButton) findViewById(R.id.showOnMap);

        final Restaurant restaurant = new DatabaseHandler(this).getRestaurant(id);

        restaurantName.setText(restaurant.getName());
        restaurantDescription.setText(restaurant.getDescription());
        restaurantLocation.setText(restaurant.getAddress());

        showOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("Address", restaurant.getAddress());
                intent.putExtra("Longitude", restaurant.getLongitude());
                intent.putExtra("Latitude",restaurant.getLatitude());
                intent.putExtra("Name", restaurant.getName());
                startActivity(intent);
            }
        });

    }

}